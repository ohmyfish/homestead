Useful command for **[Homestead][homestead]** by **[Laravel][laravel]**.

# <img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" width="28px" height="28px"/> homestead

A plugin for [Oh My Fish][oh-my-fish]. 

[![MIT License](https://img.shields.io/badge/License-MIT-blue?style=for-the-badge)](LICENSE.md)
[![Fish Shell](https://img.shields.io/badge/fish-3.1.0-blue?style=for-the-badge)](https://fishshell.com)
[![Oh My Fish](https://img.shields.io/badge/Oh%20My%20Fish-Fishshell--Framework-blue?style=for-the-badge)](https://fishshell.com)

## Prerequisite

Homestead needs to be installed in the `~/Homestead` directory as for now.

## Install

```shell script
$ omf install homestead
```

## Usage

Without the need to go into the Homestead folder execute the `vagrant` command, ex for `vagrant ssh`:

```shell script
$ homestead ssh
```

# License

[MIT][mit] © [Marc-André Appel][author]

[homestead]: https://laravel.com/docs/7.x/homestead
[laravel]: https://laravel.com
[oh-my-fish]: https://www.github.com/oh-my-fish/oh-my-fish
[author]: https://gitlab.com/marc-andre
[mit]: /LICENSE.md
