function homestead -d "Open the homestead folder and launch vagrant with the command"
    pushd $HOME/Homestead
    vagrant $argv
    popd
end
